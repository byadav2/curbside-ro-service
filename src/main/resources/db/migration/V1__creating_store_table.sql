create TABLE IF NOT EXISTS store (
  id BIGINT,
  shop VARCHAR,
  address VARCHAR,
  latitude DOUBLE,
  longitude DOUBLE,
  day_from VARCHAR,
  day_to VARCHAR,
  time_from TIME,
  time_to TIME,
  customer_per_slot INT,
  collect_time_mins INT,
  PRIMARY KEY(id)
);


