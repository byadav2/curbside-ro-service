insert into store(id, shop, address, latitude, longitude, day_from, day_to, time_from, time_to, customer_per_slot,
                  collect_time_mins)
values (101, 'Sodimac Homecenter Independencia',
        'Mall Barrio Independencia, Avenida Independencia 565, Independencia, Chile', -33.424214, -70.653809, 'MONDAY',
        'FRIDAY', '09:00:00', '18:00:00', 1, 10);

insert into store(id, shop, address, latitude, longitude, day_from, day_to, time_from, time_to, customer_per_slot,
                  collect_time_mins)
values (102, 'Falabella Lyon',
        'Nueva de Lyon 064, Providencia, Región Metropolitana', -33.421147, -70.610579, 'MONDAY',
        'SUNDAY', '09:00:00', '22:00:00', 2, 10);

insert into store(id, shop, address, latitude, longitude, day_from, day_to, time_from, time_to, customer_per_slot,
                  collect_time_mins)
values (103, 'Tottus Nataniel Cox',
        'Nataniel Cox 620, Santiago, Región Metropolitana', -33.45378, -70.652074, 'WEDNESDAY',
        'SUNDAY', '09:00:00', '19:00:00', 1, 10);

insert into store(id, shop, address, latitude, longitude, day_from, day_to, time_from, time_to, customer_per_slot,
                  collect_time_mins)
values (104, 'Falabella - Parque Arauco',
        'Av. Pdte. Kennedy 5413, Las Condes, Región Metropolitana', -33.36671, -70.49689, 'TUESDAY',
        'THURSDAY', '09:00:00', '20:00:00', 1, 10);

insert into store(id, shop, address, latitude, longitude, day_from, day_to, time_from, time_to, customer_per_slot,
                  collect_time_mins)
values (105, 'Sodimac Homecenter Los Dominicos',
        'Avenida Padre Hurtado Sur 875, Las Condes, Región Metropolitana', -33.416047, -70.540286, 'FRIDAY',
        'SUNDAY', '09:00:00', '20:00:00', 1, 10);
