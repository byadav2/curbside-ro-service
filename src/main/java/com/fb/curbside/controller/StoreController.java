package com.fb.curbside.controller;

import java.util.ArrayList;
import java.util.List;

import com.fb.curbside.domain.Store;
import com.fb.curbside.dto.StoreResponse;
import com.fb.curbside.service.StoreService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/store")
@Slf4j
public class StoreController {
    @Autowired
    StoreService storeService;

    @GetMapping("/all")
    public ResponseEntity<List<Store>> getAllStores() {
        final List<Store> result = new ArrayList<>();
        result.addAll(storeService.getAllStore());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<StoreResponse> getById(@PathVariable("id") long id) {
        final StoreResponse result = storeService.getById(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity createStore(@RequestBody Store store) {
        storeService.createStore(store);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity updateStore(@RequestBody Store store) {
        storeService.updateStore(store);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
