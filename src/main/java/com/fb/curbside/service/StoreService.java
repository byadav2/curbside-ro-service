package com.fb.curbside.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.fb.curbside.domain.Store;
import com.fb.curbside.dto.StoreResponse;
import com.fb.curbside.dto.StoreTimingDetail;
import com.fb.curbside.repository.StoreRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class StoreService {
    private final StoreRepository storeRepository;

    public StoreService(StoreRepository storeRepository) {
        this.storeRepository = storeRepository;
    }

    public List<Store> getAllStore() {
        return storeRepository.findAll();
    }

    public StoreResponse getById(Long id) {
        Optional<Store> optionalStore = storeRepository.findById(id);
        if (optionalStore.isPresent()) {
            Store store = optionalStore.get();
            LocalDateTime today = LocalDateTime.now();
            int[] allowedDays = new int[7];
            int startDay = store.getDayFrom().getValue() - 1;
            int endDay = store.getDayTo().getValue() - 1;
            for (int i = startDay; i <= endDay; i++) {
                allowedDays[i] = 1;
            }
            List<StoreTimingDetail> storeTimingDetails = new ArrayList<>();
            for (int i = 0; i < 7; i++) {
                while (allowedDays[today.getDayOfWeek().getValue() - 1] == 0) {
                    today = today.plusDays(1);
                }
                storeTimingDetails.add(StoreTimingDetail.builder().date(today.toLocalDate())
                        .dayStartTime(today.toLocalDate() + " " + store.getTimeFrom())
                        .dayEndTime(today.toLocalDate() + " " + store.getTimeTo())
                        .build());
                today = today.plusDays(1);
            }
            StoreResponse storeResponse = StoreResponse.builder()
                    .id(store.getId())
                    .shop(store.getShop())
                    .address(store.getAddress())
                    .collectTimeInMillisec(store.getCollectTimeMins() * 60000)
                    .customerPerSlot(store.getCustomerPerSlot())
                    .latitude(store.getLatitude())
                    .longitude(store.getLongitude())
                    .storeTimingDetails(storeTimingDetails)
                    .build();
            return storeResponse;
        }
        return null;
    }

    public void createStore(Store store) {
        storeRepository.save(store);
    }

    public void updateStore(Store store) {
        Optional<Store> optionalStore = storeRepository.findById(store.getId());
        if (optionalStore.isPresent()) {
            storeRepository.save(store);
        }
    }
}
