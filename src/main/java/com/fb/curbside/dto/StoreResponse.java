package com.fb.curbside.dto;

import java.time.LocalTime;
import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class StoreResponse {
    private long id;
    private int customerPerSlot;
    private int collectTimeInMillisec;
    private String shop;
    private String address;
    private double latitude;
    private double longitude;
    private List<StoreTimingDetail> storeTimingDetails;
}
