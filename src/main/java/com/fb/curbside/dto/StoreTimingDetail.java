package com.fb.curbside.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class StoreTimingDetail {
    LocalDate date;
    String dayStartTime;
    String dayEndTime;
}
