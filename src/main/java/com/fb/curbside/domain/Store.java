package com.fb.curbside.domain;

import java.time.DayOfWeek;
import java.time.LocalTime;

import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Store extends BaseEntity {
    private String shop;
    private String address;
    private double latitude;
    private double longitude;
    private LocalTime timeFrom;
    private LocalTime timeTo;
    private DayOfWeek dayFrom;
    private DayOfWeek dayTo;
    private int customerPerSlot;
    private int collectTimeMins;
}